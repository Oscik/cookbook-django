from django.http import HttpResponseRedirect
from django.views.generic import DetailView, ListView

from recipes import models


class RecipeListView(ListView):
    model = models.Recipe

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['elements'] = models.Element.objects.all()
        return context


class RecipeDetailView(DetailView):
    model = models.Recipe

    def dispatch(self, request, *args, **kwargs):
        obj = self.get_object()
        slug_from_url = self.kwargs.get(self.slug_url_kwarg, '')
        if obj.slug != slug_from_url:
            return HttpResponseRedirect(obj.get_absolute_url())
        return super(RecipeDetailView, self).dispatch(request, *args, **kwargs)
