from adminsortable2.admin import SortableAdminMixin, SortableInlineAdminMixin
from dal_select2 import widgets as dal_widgets
from django import forms
from django.contrib import admin

from recipes import models


class IngredientForm(forms.ModelForm):
    class Meta:
        widgets = dict(
            element=dal_widgets.Select2(),
            quantity=dal_widgets.Select2(),
        )


class IngredientInline(SortableInlineAdminMixin, admin.TabularInline):
    model = models.Ingredient
    form = IngredientForm
    extra = 0


class SectionInline(SortableInlineAdminMixin, admin.TabularInline):
    model = models.Section
    extra = 0


@admin.register(models.Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug': ('name',)}


class RecipeForm(forms.ModelForm):
    class Meta:
        widgets = dict(
            category=dal_widgets.Select2(),
        )


@admin.register(models.Recipe)
class RecipeAdmin(SortableAdminMixin, admin.ModelAdmin):
    form = RecipeForm
    inlines = [IngredientInline, SectionInline]
    filter_horizontal = ['tags', ]


@admin.register(models.Quantity)
class QuantityAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ['name', ]


@admin.register(models.Element)
class ElementAdmin(SortableAdminMixin, admin.ModelAdmin):
    filter_horizontal = ['quantity']


@admin.register(models.PreparationTime)
class PreparationTimeAdmin(admin.ModelAdmin):
    pass
