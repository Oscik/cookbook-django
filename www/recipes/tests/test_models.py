from decimal import Decimal

from django.test import TestCase

from recipes import models
from recipes.tests import factories


class TagTestCase(TestCase):
    def test_create(self):
        factories.TagFactory.create()
        self.assertEqual(models.Tag.objects.count(), 1)

    def test_str(self):
        tag = factories.TagFactory.create()
        self.assertEqual(str(tag), 'name 1')


class RecipeTestCase(TestCase):
    def test_slug(self):
        recipe = factories.RecipeFactory.create()
        self.assertEqual(recipe.slug, 'test-recipe-name')

    def test_get_absolute_url(self):
        recipe = factories.RecipeFactory.create()
        self.assertEqual(recipe.get_absolute_url(), '/test-recipe-name/1/')

    def test_url(self):
        recipe = factories.RecipeFactory.create()
        self.assertEqual(recipe.url, '/api/recipes/1/')

    def test_author_name(self):
        recipe = factories.RecipeFactory.create()
        self.assertEqual(recipe.author_name, 'John Doe')

    def test_ingredients(self):
        recipe = factories.RecipeFactory.create()
        factories.IngredientFactory.create(recipe=recipe)
        factories.IngredientFactory.create(recipe=recipe)
        self.assertEqual(len(recipe.ingredients), 2)

    def test_ingredients_json(self):
        recipe = factories.RecipeFactory.create()
        factories.IngredientFactory.create(recipe=recipe)
        factories.IngredientFactory.create(recipe=recipe)
        self.assertEqual(len(recipe.ingredients_json), 2)
        self.assertIn('some element name ', recipe.ingredients_json[0]['element_name'])
        self.assertEqual(recipe.ingredients_json[0]['quantity_name'], 'gram')
        self.assertEqual(recipe.ingredients_json[0]['amount'], Decimal('10.00'))

    def test_element_slugs(self):
        recipe = factories.RecipeFactory.create()
        factories.IngredientFactory.create(recipe=recipe)
        self.assertEqual(len(recipe.element_slugs), 1)
        self.assertIn('some-element-name-', recipe.element_slugs[0])


class ElementTestCase(TestCase):
    def test_create(self):
        factories.ElementFactory.create()
        self.assertEqual(models.Element.objects.count(), 1)

    def test_slug(self):
        element = factories.ElementFactory.create()
        self.assertIn('some-element-name-', element.slug)
