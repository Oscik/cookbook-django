import factory
from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile

from recipes import models
from users.tests import factories as users_factories


class TagFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Tag

    name = factory.Sequence(lambda n: 'name {}'.format(n))
    slug = factory.Sequence(lambda n: 'name-{}'.format(n))


class RecipeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Recipe
        django_get_or_create = ['name']

    name = 'Test recipe name'
    author = factory.SubFactory(users_factories.UserFactory)
    preparation_time = 60

    @factory.lazy_attribute
    def image(self):
        upload_file = open(settings.TEST_IMAGE_PATH, 'rb')
        return SimpleUploadedFile(upload_file.name, upload_file.read())


class ElementFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Element

    name = factory.Sequence(lambda n: 'some element name {}'.format(n))


class QuantityFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Quantity

    name = 'gram'


class IngredientFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Ingredient

    recipe = factory.SubFactory(RecipeFactory)
    element = factory.SubFactory(ElementFactory)
    quantity = factory.SubFactory(QuantityFactory)
    amount = 10


class SectionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Section

    recipe = factory.SubFactory(RecipeFactory)
    title = 'Some section title'
    content = 'Some section content'

    @factory.lazy_attribute
    def image(self):
        upload_file = open(settings.TEST_IMAGE_PATH, 'rb')
        return SimpleUploadedFile(upload_file.name, upload_file.read())
