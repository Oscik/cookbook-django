from django.test import TestCase
from django.urls import reverse

from recipes.tests import factories


class RecipesListViewTestCase(TestCase):
    def test__get__returns_recipes(self):
        factories.RecipeFactory.create()
        url = reverse('recipes:list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Test recipe name')


class RecipeDetailTestCase(TestCase):
    def test_detail_redirect(self):
        factories.RecipeFactory.create(id=1)
        url = '/some-other-slug/1/'
        response = self.client.get(url)
        self.assertRedirects(response, '/test-recipe-name/1/')

    def test_detail(self):
        recipe = factories.RecipeFactory.create()
        response = self.client.get(recipe.get_absolute_url())
        self.assertContains(response, 'Test recipe name')
