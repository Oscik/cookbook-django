from django.conf import settings
from django.db import models
from django.db.models import F
from django.utils.functional import cached_property
from django.utils.text import slugify
from rest_framework.reverse import reverse as reverse_drf
from versatileimagefield.fields import VersatileImageField, PPOIField


class Tag(models.Model):
    name = models.CharField(unique=True, max_length=100)
    slug = models.SlugField(unique=True, max_length=100)

    def __str__(self):
        return self.name


class Recipe(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='recipes')
    name = models.CharField(max_length=255)
    image = VersatileImageField(upload_to='recipe', null=True, ppoi_field='ppoi')
    ppoi = PPOIField()
    content = models.TextField()
    preparation_time = models.SmallIntegerField()
    added = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    is_visible = models.BooleanField(default=False)
    is_deleted = models.BooleanField(default=False)
    is_draft = models.BooleanField(default=False)
    tags = models.ManyToManyField('recipes.Tag', related_name='recipes', blank=True)

    class Meta:
        ordering = ['id', ]

    def __str__(self):
        return self.name

    @cached_property
    def slug(self):
        return slugify(self.name)

    def get_absolute_url(self):
        return '/{}/{}/'.format(self.slug, self.id)

    @cached_property
    def url(self):
        return reverse_drf('api:recipes-detail', args=[self.id])

    @cached_property
    def author_name(self):
        return self.author.get_full_name()

    @cached_property
    def ingredients(self):
        return self.ingredient_set.all()

    @cached_property
    def ingredients_json(self):
        return self.ingredient_set.annotate(
            element_name=F('element__name'), quantity_name=F('quantity__name')
        ).values('element_name', 'quantity_name', 'amount')

    @cached_property
    def element_slugs(self):
        return [ingredient.element.slug for ingredient in self.ingredients]


class Section(models.Model):
    recipe = models.ForeignKey('recipes.Recipe', on_delete=models.CASCADE)
    image = VersatileImageField(upload_to='sections', blank=True, ppoi_field='ppoi')
    ppoi = PPOIField()
    title = models.CharField(max_length=255, blank=True)
    content = models.TextField(blank=True)
    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta:
        ordering = ['order', ]

    def __str__(self):
        return '{} {}'.format(self.recipe, self.order)


class Quantity(models.Model):
    name = models.CharField(max_length=32)
    last_modified = models.DateTimeField(auto_now=True)
    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "quantities"
        ordering = ['order', ]


class Element(models.Model):
    name = models.CharField(max_length=32)
    quantity = models.ManyToManyField('recipes.Quantity', related_name='quantities')
    last_modified = models.DateTimeField(auto_now=True)
    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta:
        ordering = ['order']

    def __str__(self):
        return self.name

    @cached_property
    def slug(self):
        return slugify(self.name)


class Ingredient(models.Model):
    recipe = models.ForeignKey('recipes.Recipe', on_delete=models.CASCADE)
    element = models.ForeignKey('recipes.Element', on_delete=models.CASCADE)
    quantity = models.ForeignKey('recipes.Quantity', on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=8, decimal_places=2)
    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    def __str__(self):
        return "{} {}".format(self.recipe.name, self.element)

    class Meta:
        unique_together = ("recipe", "element")
        ordering = ['order', ]


class PreparationTime(models.Model):
    text = models.CharField(max_length=32)
    min_time = models.PositiveSmallIntegerField()
    max_time = models.PositiveSmallIntegerField()
    last_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.text
