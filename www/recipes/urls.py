from django.urls import path

from recipes import views

urlpatterns = [
    path('', views.RecipeListView.as_view(), name='list'),
    path('<slug:slug>/<int:pk>/', views.RecipeDetailView.as_view(), name='detail'),
]
