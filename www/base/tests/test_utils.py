from django.test import TestCase

from base import utils


class UtilsTestCase(TestCase):
    def test_get_schema(self):
        self.assertEqual(utils.get_schema(), 'http')

    def test_get_url(self):
        self.assertEqual(utils.get_url(), 'http://example.com')

    def test_get_full_url(self):
        self.assertEqual(utils.get_full_url('/some-url'), 'http://example.com/some-url')
