from django.test import TestCase

from base.management.commands.populate_sample_data import Command
from recipes import models as recipes_models


class PopulateTestCase(TestCase):
    def test_populate(self):
        Command().handle()
        self.assertEqual(10, recipes_models.Recipe.objects.count())
