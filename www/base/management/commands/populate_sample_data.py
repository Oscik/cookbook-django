from django.core.management import BaseCommand

from recipes.tests import factories as recipes_factories


class Command(BaseCommand):
    def handle(self, *args, **options):
        for i in range(10):
            recipes_factories.RecipeFactory.create(name='Test name {}'.format(i))
