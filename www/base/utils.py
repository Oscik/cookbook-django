from django.conf import settings
from django.contrib.sites.models import Site


def get_schema():
    return 'https' if settings.USE_SSL else 'http'


def get_url():
    return '{}://{}'.format(get_schema(), Site.objects.get_current().domain)


def get_full_url(url):
    return '{}{}'.format(get_url(), url)
