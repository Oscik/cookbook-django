from rest_framework import routers
from api import views


router = routers.DefaultRouter()
router.register(r'recipes', views.RecipeViewSet, basename='recipes')
router.register(r'elements', views.ElementViewSet)

urlpatterns = router.urls
