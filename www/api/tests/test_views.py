from rest_framework.test import APITestCase

from recipes.tests import factories


class RecipesListTestCase(APITestCase):
    def test_view(self):
        factories.RecipeFactory.create()
        url = '/api/recipes/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)


class RecipesDetailTestCase(APITestCase):
    def test_view(self):
        recipe = factories.RecipeFactory.create()
        url = recipe.url
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 10)


class ElementsTestCase(APITestCase):
    def test_view(self):
        factories.ElementFactory.create()
        url = '/api/elements/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
