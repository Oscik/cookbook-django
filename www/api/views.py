from rest_framework import viewsets

from api import serializers
from recipes import models


class RecipeViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = models.Recipe.objects.all()
    serializer_action_classes = dict(
        list=serializers.RecipeListSerializer,
        retrieve=serializers.RecipeDetailSerializer,
    )

    def get_serializer_class(self):
        return self.serializer_action_classes[self.action]


class ElementViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = models.Element.objects.all()
    serializer_class = serializers.ElementSerializer
