from rest_framework import serializers
from versatileimagefield.serializers import VersatileImageFieldSerializer

from recipes import models


class RecipeListSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='api:recipes-detail')
    image = VersatileImageFieldSerializer(
        sizes=[
            ('full', 'url'),
            ('thumbnail_list', 'crop__160x120'),
        ]
    )

    class Meta:
        model = models.Recipe
        fields = ('id', 'url', 'name', 'slug', 'image', 'element_slugs')


class SectionSerializer(serializers.ModelSerializer):
    image = VersatileImageFieldSerializer(
        sizes=[
            ('full', 'url'),
            ('thumbnail_detail', 'thumbnail__800x10000'),
        ]
    )

    class Meta:
        model = models.Section
        fields = ('id', 'title', 'content', 'image',)


class RecipeDetailSerializer(serializers.ModelSerializer):
    ingredients = serializers.ListField(source='ingredients_json')
    sections = SectionSerializer(source='section_set', many=True)
    image = VersatileImageFieldSerializer(
        sizes=[
            ('full', 'url'),
            ('thumbnail_detail', 'thumbnail__1024x768'),
        ]
    )

    class Meta:
        model = models.Recipe
        fields = (
            'author_name',
            'name',
            'slug',
            'content',
            'preparation_time',
            'added',
            'last_modified',
            'image',
            'ingredients',
            'sections',
        )


class ElementSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Element
        fields = ('name', 'slug')
