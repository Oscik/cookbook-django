from .settings_base import *
from .settings_live_postgres import DATABASES
from .settings_live_raven import RAVEN_CONFIG
from .settings_live_secret_key import SECRET_KEY


DEBUG = False

SITE_ID = 1
USE_SSL = True

INSTALLED_APPS += [
    'raven.contrib.django.raven_compat',
]
